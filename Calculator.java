
public class Calculator
{
    /* add n2 to n1 */
    static double add(double n1, double n2)
    {
        return (n1 + n2);
    }

    /* subtract n2 from n1 */
    static double subtract(double n1, double n2)
    {
        return n1 - n2;
    }

    /* multiply n1 by n2 */
    static double multiply(double n1, double n2)
    {
        double result = 0;
        result = n1 * n2;
        return result;
    }

    /* divide n1 by n2 */
    static double divide(double n1, double n2)
    {
        double result = 0;
        try{
	    result = n1 / n2;
	}catch (Exception ex){
	}
        
        return result;
    }

    /* calculate the absolute value of n */
    static double abs(double n)
    {
        if (n <= 0){
	    n = -n;
	}
        return n;
    }

    /* raise n to the power of m */
    static double power(double n, int m)
    {
        double result = n;
        for (int i = 1; i < m; ++i){
	    result = result*n;
	}
        return result;
    }
}
